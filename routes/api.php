<?php

use App\Http\Controllers\TransactionController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('transactions' , [TransactionController::class, 'store'])->name('api.transactions.store');

Route::post('callback' , [TransactionController::class, 'callback'])->name('api.transactions.callback');


Route::get('summary' , [TransactionController::class, 'summary'])->name('api.transactions.summary');
Route::get('downloadDocument' , [TransactionController::class, 'download_document'])->name('api.transactions.download_document');




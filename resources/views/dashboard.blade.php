<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <style>

        .header {
            background-color: #343a40;
            color: #fff;
            text-align: center;
            padding: 20px 0;
            margin-bottom: 30px;
        }

        .footer {
            background-color: #343a40;
            color: #fff;
            text-align: center;
            padding: 10px 0;
            position: fixed;
            bottom: 0;
            width: 100%;
        }
        .form-check-input {
            appearance: none;
            -webkit-appearance: none;
            -moz-appearance: none;
            width: 20px;
            height: 20px;
            border: 2px solid #ccc;
            border-radius: 5px;
            position: relative;
            cursor: pointer;
            outline: none;
        }

        .form-check-input:checked {
            background-color: #007bff;
            border-color: #007bff;
        }

        .form-check-input:checked::after {
            content: "\2713";
            font-size: 14px;
            color: #fff;
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
        }


        .form-check-label {
            margin-left: 10px;
        }


        .btn {
            display: inline-block;
            background-color: #62cb1e;
            color: #fff;
            border: none;
            padding: 10px 20px;
            border-radius: 5px;
            cursor: pointer;
            text-decoration: none;
            transition: background-color 0.3s;
        }

        .btn:hover {
            background-color: #0056b3;
        }

        .styled-form {
            max-width: 500px;
            margin: 0 auto;
            padding: 20px;
            background-color: #f9f9f9;
            border-radius: 8px;
        }

        .form-group {
            margin-bottom: 20px;
        }

        label {
            font-weight: bold;
        }
        .form-control {
            width: 100%;
            padding: 10px;
            border: 1px solid #ccc;
            border-radius: 5px;
            box-sizing: border-box;
        }
        .control-label {
            font-weight: bold;
        }


        #signersContainer {
            margin-top: 20px;
        }

        .signer {
            border: 1px solid #ccc;
            padding: 15px;
            border-radius: 8px;
            margin-bottom: 20px;
        }
        .btn-primary {
            margin-top: 20px;
        }
    </style>

</head>
<body>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.3/dist/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>


<div class="header">
    <h2>ValidSign</h2>

</div>

<div class="container my-5">

    <div class="">
        <h5>NEW VALIDSIGN REQUEST</h5>
        <h1>Create New Transaction</h1>
    </div>
    <form id="transactionForm" action="{{ route('api.transactions.store') }}" method="post"
          enctype="multipart/form-data">
        @csrf
        @method('post')
        <div class="form-group">
            <label for="transactionName">Transaction Name</label>
            <input type="text" name="name" class="form-control" id="transactionName"
                   placeholder="Enter transaction name">
        </div>
        <div class="form-group">
            <label for="file" class="col-sm-2 control-label">Upload</label>
            <div class="form-group">
                <input type="file" class="form-control" name="documents[]" id="photo" onchange="readFile(this);"
                       multiple>

            </div>
        </div>
        <div id="status"></div>
        <div id="photos" class="row"></div>
        <div class="mt-5">
            <h2>Add Multiple Signers</h2>
            <div id="signersContainer">
                <div class="form-group signer">
                    <label for="addSigner">Signer Information:</label>
                    <input type="text" class="form-control" name="roles[0][signers][0][firstName]"
                           placeholder="Signer firstname"><br>
                    <input type="text" class="form-control" name="roles[0][signers][0][lastName]"
                           placeholder="Signer lastname"><br>
                    <input type="email" class="form-control" name="roles[0][signers][0][email]"
                           placeholder="Signer email" required>
<br>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="sms_verification_signer1" value="1"
                               id="defaultCheck1">
                        <label class="form-check-label" for="defaultCheck1">
                            sms verfication
                        </label>
                    </div>

                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="iDIN_signer1" value="1"
                               id="defaultCheck1">
                        <label class="form-check-label" for="defaultCheck1">
                            iDIN verification
                        </label>
                    </div>
                </div>

                <input type="hidden" name="type" value="PACKAGE">
                <input type="hidden" name="language" value="en">
                <input type="hidden" name="status" value="DRAFT">
                <input type="hidden" name="autocomplete" value="true">

                <div class="form-group signer">
                    <label for="addSigner">Signer Information:</label>
                    <input type="text" class="form-control" name="roles[1][signers][0][firstName]"
                           placeholder="Signer firstname"><br>
                    <input type="text" class="form-control" name="roles[1][signers][0][lastName]"
                           placeholder="Signer lastname"><br>
                    <input type="email" class="form-control" name="roles[1][signers][0][email]"
                           placeholder="Signer email" required>
                    <br>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="sms_verification_signer2" value="1"
                               id="defaultCheck1">
                        <label class="form-check-label" for="defaultCheck1">
                            sms verfication
                        </label>
                    </div>

                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="iDIN_signer2" value="1"
                               id="defaultCheck1">
                        <label class="form-check-label" for="defaultCheck1">
                            iDIN verification
                        </label>
                    </div>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="signing_order" value="1" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">
                        enable signing order
                    </label>
                </div>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="checkbox" name="me_as_signer" value="1" id="defaultCheck1">
                <label class="form-check-label" for="defaultCheck1">
                    add myself (authorised user) to the transaction
                </label>
            </div>

            <div class="form-check">
                <input class="form-check-input" type="checkbox" name="designer_session" value="1" id="defaultCheck1">
                <label class="form-check-label" for="defaultCheck1">
                    Designer session
                </label>
            </div>


        </div>
        <br>
        <button type="submit" class="btn btn-primary">Submit</button>

    </form>


        <br>
        <div class="row">
            <div class="col-md-6">
                <form id="transactionForm" action="{{ route('api.transactions.callback') }}" method="POST"  enctype="multipart/form-data">
                    @csrf

                    <button type="submit" class="btn btn-primary">Download Summary</button>
                </form>
            </div>
            <div class="col-md-6">
                <form id="transactionForm" action="{{ route('api.transactions.download_document') }}" method="get">
                    <button type="submit" class="btn btn-primary">Download Document</button>
                </form>
            </div>
        </div>






</div>

<div class="footer">
    &copy; ValidSign
</div>


<script>
    function readFile(input) {
        $("#status").html('Processing...');
        counter = input.files.length;
        for (x = 0; x < counter; x++) {
            if (input.files && input.files[x]) {

                var reader = new FileReader();

                reader.onload = function (e) {
                    $("#photos").append('<div class="col-md-3 col-sm-3 col-xs-3"><img src="' + e.target.result + '" class="img-thumbnail"></div>');
                };

                reader.readAsDataURL(input.files[x]);
            }
        }
        if (counter == x) {
            $("#status").html('');
        }
    }


    // $(document).ready(function() {
    //
    //     $('#addSignerBtn').click(function() {
    //         var signerHtml = $('#signersContainer .signer').first().clone();
    //
    //         signerHtml.find('input[type="text"], input[type="email"]').val('');
    //         $('#signersContainer').append(signerHtml);
    //     });
    // });
</script>
</body>
</html>

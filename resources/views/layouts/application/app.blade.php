@extends('layouts.public.base')

@section('app-content')
    <div class="bs-canvas-overlay bs-canvas-anim bg-dark position-fixed w-100 h-100"></div>
    <nav class="navbar navbar-expand-md shadow-sm">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                <img src="{{ asset('/assets/images/logo.png') }}" class="mw200">
            </a>
            <a href="#" id="navbar-toggler" class="navbar-toggler" data-toggle="canvas" data-target="#nav-sidebar"
               aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <ion-icon name="menu-outline"></ion-icon>
            </a>

            @include('layouts.application.navigation')

        </div>
    </nav>

    <div class="page-overlay"></div>
    <div class="spanner">
        <div class="loader"></div>
        <div class="loader-text">
            @lang('global.one_moment_please')
        </div>
    </div>

    <main class="py-4">
        @include('includes.if_errors')
        @yield('content')
    </main>

@endsection

@push('body-scripts')
    <script type="module" src="{{ mix('assets/ionicons/ionicons.esm.js') }}"></script>
    <script nomodule src="{{ mix('assets/ionicons/ionicons.js') }}"></script>
    @include('includes.scripts.loading')
    @include('includes.scripts.toastr')
@endpush

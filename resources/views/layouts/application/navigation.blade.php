@php use App\View\Components\Notification; @endphp
<div class="collapse navbar-collapse" id="navbarSupportedContent">
@auth
    @include('layouts.application.navigation.mainmenu')
@endauth

    <div class="navbar-nav dropdown ml-auto">

    </div>

    <!-- Right Side Of Navbar -->
    <ul class="navbar-nav ml-auto">

        <x-notification />

        <!-- Authentication Links -->
        <li class="nav-item dropdown">
            <a id="navbarDropdown" class="language" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <ion-icon name="earth-outline" class="fsize28 font-weight-light valign-middle"></ion-icon>
            </a>

            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <form method="POST" action="{{ route('locale.post') }}">
                    @csrf
                    <input type="hidden" name="language" value="en"/>
                    <button type="submit" class="dropdown-item">@lang('navigation.english')</button>
                </form>
                <form method="POST" action="{{ route('locale.post') }}">
                    @csrf
                    <input type="hidden" name="language" value="nl"/>
                    <button type="submit" class="dropdown-item">@lang('navigation.dutch')</button>
                </form>
            </div>
        </li>
        @auth
            <li class="nav-item dropdown">
                <a id="navbarDropdown" class="username" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="row">
                        <span class="mr-2">
                            <i class="bx bx-user-circle fsize29 font-weight-lighter"></i>
                        </span>
                        <span class="d-xs-none d-sm-none d-md-none d-lg-none d-xl-block text-nowrap">
                            {{ Auth::user()->name() }}
                        </span>
                    </span>
                </a>

                @include('layouts.application.navigation.dropdown')

            </li>
        @endguest
    </ul>
</div>

{{--Sidebar Navigation mobile view--}}
<div id="nav-sidebar" class="bs-canvas bs-canvas-anim bs-canvas-right position-fixed bg-white h-100">
    <header class="bs-canvas-header p-4 bg-white overflow-auto">
        <button type="button" class="bs-canvas-close float-right" aria-label="@lang('global.close')">
            <span class="round round-s round-primary text-primary">
                <ion-icon name="close-outline" class="fsize16 mt-1"></ion-icon>
            </span>
        </button>
        <h4 class="d-inline-block text-primary mb-0 float-left bs-canvas-header">Mobile Navigation</h4>
    </header>
    <div class="bs-canvas-content px-4 py-2">
        <div class="row">
            <div class="col-md-12">
                <ul class="navbar-nav ml-auto">

                    <x-notification displayMode="{{ Notification::MODE_MOBILE }}" />

                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="username" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <ion-icon name="earth-outline" class="fsize28 mr-2 valign-middle font-weight-light"></ion-icon> @lang('navigation.language')
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <form method="POST" action="{{ url('/locale/en') }}">
                                @csrf
                                <button type="submit" class="dropdown-item">@lang('navigation.english')</button>
                            </form>
                            <form method="POST" action="{{ url('/locale/nl') }}">
                                @csrf
                                <button type="submit" class="dropdown-item">@lang('navigation.dutch')</button>
                            </form>
                        </div>
                    </li>
                    <!-- Authentication Links -->
                    @auth
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="username" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="bx bx-user-circle fsize28 mr-2 valign-middle font-weight-light"></i> {{ Auth::user()->name() }}
                            </a>

                            @include('layouts.application.navigation.dropdown')

                        </li>
                    @endguest
                </ul>
            </div>
        </div>
        <hr>
        <div class="row mt-2">
            <div class="col-md-12">
                @auth
                   @include('layouts.application.navigation.mainmenu')
                @endauth
            </div>
        </div>
    </div>
</div>

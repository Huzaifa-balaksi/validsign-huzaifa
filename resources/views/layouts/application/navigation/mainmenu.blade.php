<div class="link-container @if(request()->routeIs('dashboard')) is-active @endif">
    <a class="link" href="{{ route('dashboard') }}">
        @lang('navigation.dashboard')
    </a>
</div>
<div class="link-container @if(request()->routeIs('transaction.index')) is-active @endif">
    <a class="link" href="{{ route('transaction.index') }}">
        @lang('navigation.transactions')
    </a>
</div>
<div class="link-container @if(request()->routeIs('organisation.index')) is-active @endif">
    <a class="link" href="{{ route('organisation.index') }}">
        @lang('navigation.organisations')
    </a>
</div>

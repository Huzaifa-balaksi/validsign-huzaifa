<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
    <a class="dropdown-item" href="{{ url('/profile') }}">@lang('navigation.profile')</a>
    @if((Auth::user())->role == 'admin')
        <a class="dropdown-item" href="{{ url('/settings') }}">@lang('navigation.settings')</a>
    @endif
    <a class="dropdown-item logoutLink" href="{{ route('logout') }}">
        @lang('navigation.logout')
    </a>

    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
        @csrf
    </form>
</div>

<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ config('app.name', 'ValidSign4Accountancy') }}</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="{{ mix('/js/app.js') }}"></script>
    <link href="{{ mix('/css/app.css') }}" rel="stylesheet">
    @stack('head-scripts')
</head>
<body>
    <div id="app">
    @yield('app-content')
    </div>
    @stack('body-scripts')
</body>
</html>

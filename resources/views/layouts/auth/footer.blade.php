<div class="support-container">
    <div class="support-title">{{ __('global.need_any_support') }}</div>
    <div class="support-link">{!! __('global.need_support_portal') !!}</div>
    <div class="support-email">{!! __('global.mail_to_support') !!}</div>
</div>
<div class="language-selector">
    <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown">
            <a id="navbarDropdown" class="username" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                <ion-icon name="earth-outline" class="fsize28 mr-2" style="vertical-align: middle; font-weight: 300"></ion-icon> @lang('navigation.language')
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <form method="POST" action="{{ route('locale.post') }}">
                    @csrf
                    <input type="hidden" name="language" value="en">
                    <button type="submit" class="dropdown-item">
                        @lang('navigation.english')
                    </button>
                </form>
                <form method="POST" action="{{ route('locale.post') }}">
                    @csrf
                    <input type="hidden" name="language" value="nl">
                    <button type="submit" class="dropdown-item">
                        @lang('navigation.dutch')
                    </button>
                </form>
            </div>
        </li>
    </ul>
</div>

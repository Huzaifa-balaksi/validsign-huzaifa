<div class="login-form-container">
    <div class="logo-container">
        <div class="logo">
            <a href="https://www.validsign.eu" target="_blank">
                <img src="{{ asset('assets/images/logo.png') }}" alt="Logo">
            </a>
        </div>
    </div>
    <div class="login-form">
        <div class="notifications">
            @include('layouts.auth.notifications')
        </div>
        <form method="POST" action="{{ route('login') }}">
            @csrf

            <div class="form-group row">
                <div class="col-md-12">
                    <label for="email" class="text-primary">{{ __('auth.email') }}</label>
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="email@example.com">

                    @error('email')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-12">
                    <label for="email" class="text-primary">{{ __('auth.password') }}</label>
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                    @error('password')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-12">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                        <label class="form-check-label text-primary pt-1 ml-2" for="remember">
                            {{ __('auth.remember_me') }}
                        </label>
                    </div>
                </div>
            </div>

            <div class="form-group row mb-4">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary">
                        {{ __('auth.login') }}
                    </button>
                </div>
            </div>

            <div class="form-group row ">
                <div class="col-md-12">
                    @if (Route::has('password.request'))
                        <a class="password-recovery-link" href="{{ route('password.request') }}">
                            {{ __('auth.forgot_password') }}
                        </a>
                    @endif
                </div>
            </div>
        </form>
    </div>
    @include('layouts.auth.footer')
</div>

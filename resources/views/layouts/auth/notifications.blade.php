@if(!empty($notifications)) 
    @foreach($notifications as $notification)
        <div class="notification-root notification-alert notification-alert-{{ $notification->type }}" role="alert">
            <div class="notification-icon">
                <ion-icon name="information-circle-outline" class="notification-icon-root notification-font-inherit"></ion-icon>
            </div>
            <div class="notification-message">
                <div class="title">{{ $notification->title }}</div>
                <div class="description">{{ $notification->description }}</div>
            </div>
        </div>
    @endforeach
@endif
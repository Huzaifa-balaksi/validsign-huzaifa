<div class="login-form-container">
    <div class="logo-container">
        <div class="logo">
            <a href="/dashboard">
                <img src="{{ asset('assets/images/logo.png') }}" alt="Logo">
            </a>
        </div>
    </div>
    <div class="login-form">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif

        <form method="POST" action="{{ route('password.email') }}">
            @csrf

            <div class="form-group row">

                <div class="col-md-12">
                    <label for="email" class="text-primary">{{ __('auth.email') }}</label>
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="email@example.com">

                    @error('email')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>
            </div>

            <div class="form-group row mb-4">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary">
                        {{ __('auth.sent_link') }}
                    </button>
                </div>
            </div>

            <div class="form-group row ">
                <div class="col-md-12">
                    <a class="password-recovery-link" href="{{ route('login') }}">
                        {{ __('global.back_to_login') }}
                    </a>
                </div>
            </div>
        </form>
    </div>
    @include('layouts.auth.footer')
</div>

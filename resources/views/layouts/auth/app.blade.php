@extends('layouts.public.base')

@section('app-content')

    <main>
        @yield('content')
    </main>

@endsection

@push('body-scripts')
    <script type="module" src="{{ mix('assets/ionicons/ionicons.esm.js') }}"></script>
    <script nomodule src="{{ mix('assets/ionicons/ionicons.js') }}"></script>
@endpush

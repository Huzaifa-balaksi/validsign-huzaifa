<div class="login-news">
    <div class="container-fluid px-0">
        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner" role="listbox">
                @if(!empty($news))
                    @foreach($news->items as $item)
                    <div class="carousel-item @if($item == $news->items[0]) active @endif">
                            <div class="d-flex justify-content-center align-items-end min-vh-60" style="background-image: url('{{ $item->background }}'); background-size:100% 100%;">
                                <img src="{{ $item->image }}" class="mb-5">
                            </div>
                            <div class="container py-4 text-center mr-auto ml-auto">
                                <div class="news-title">{!! $item->title !!}</div>
                                <div class="news-description align-items-center">{!! $item->description !!}</div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">{{ __('global.previous') }}</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">{{ __('global.next') }}</span>
            </a>
        </div>
    </div>
</div>

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreTransactionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [

            'name' => 'required',
            'type' => 'required',
            'language' => 'required',
            'status' => 'required',

            'me_as_signer' => 'bool',

            'sms_verification_signer1' => 'bool',
            'sms_verification_signer2' => 'bool',

            'signing_order' => 'bool',

            'iDIN_signer1' => 'bool',
            'iDIN_signer2' => 'bool',

            'download_document' => 'bool',
            'designer_session' => 'bool',
            'documents' => 'array',

            'roles' => 'required',
            'signer.*.firstName' => 'required|max:50',
            'signer.*.lastName' => 'required|max:50',
            'signer.*.email' => 'required|email'




        ];
    }
}

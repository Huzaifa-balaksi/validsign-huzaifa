<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;

class connectionController extends Controller
{
    public function validsign_connection_function()
    {

        $client = new \GuzzleHttp\Client();

        $headers = [
            'Authorization' => 'Basic ' . env('VALIDSIGN_API_KEY'),
            'Accept' => 'application/json',
        ];

        $request = $client->get('https://try.validsign.eu/api/session', [
            'headers' => $headers
        ]);

        $response = $request->getBody();

        dd($response);
    }
}





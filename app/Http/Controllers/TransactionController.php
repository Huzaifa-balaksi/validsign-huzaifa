<?php

namespace App\Http\Controllers;


use App\Http\Requests\StoreTransactionRequest;
use Dflydev\DotAccessData\Data;
use http\Message;
use Illuminate\Http\Client\PendingRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

use Illuminate\Support\Facades\Response;


class TransactionController extends Controller
{
    /**
     * Create a new Package in ValidSign
     */

    public function store(StoreTransactionRequest $request)
    {

        $requestData = $request->validated();
        $file = $request->file('documents')[0];
        $fileName = $file->getClientOriginalName();

        $documents =
            json_encode([
                "name" => $fileName,
                "extract" => true,
                "extractionTypes" => [
                    "TEXT_TAGS"
                ]
            ]);

        session(['documents' => $documents]);

        unset($requestData['documents']);

        $requestData['status'] = 'DRAFT';
        $requestData['autocomplete'] = true;

        $this->signing_order($requestData);

        $this->sms_verification($requestData);

        $jsonData = json_encode($requestData);

        // creating a new package

        $response = $this->makeRequest()->asMultipart()->attach(
            name: "payload",
            contents: $jsonData
        )->post('/api/packages');


        $packageId = json_decode($response->body())->id ?? null;

        // adding document to package
        $this->docuemnts($file, $documents, $packageId);
        // attach iDIN to signer
        $this->iDIN($packageId, $requestData);
        // adding authorize user to the transaction
        $this->me_as_signer($requestData, $packageId);

        // Designer session
        if (isset($requestData['designer_session'])) {
            return $this->signerSession($packageId);
        }

        return 'Transaction added successfully';

    }

    private function docuemnts($file, $documents, $packageId)
    {
        $this->makeRequest()->asMultipart()
            ->accept('text/html')
            ->attach(
                name: "file",
                contents: fopen($file->getRealPath(), 'r')
            )
            ->attach(
                name: 'payload',
                contents: $documents
            )
            ->post('/api/packages/' . $packageId . '/documents');

    }

    private function iDIN($packageId, $requestData)
    {
//        dd($requestData);
        $response = $this->makeRequest()->get('/api/packages/' . $packageId . '/roles/');
//dd($response->body());
        if (isset($requestData['iDIN_signer1'])) {
            $roleId = $response->json('results')[1]['id'];
            //dd($packageId);
            $response = $this->makeRequest()
                ->post('/api/packages/' . $packageId . '/roles/' . $roleId . '/verification', ['typeId' => 'iDIN', 'payload' => '']);

        }
        if (isset($requestData['iDIN_signer2'])) {

            $roleId = $response->json('results')[2]['id'];

            $response = $this->makeRequest()
                ->post('/api/packages/' . $packageId . '/roles/' . $roleId . '/verification', ['typeId' => 'iDIN', 'payload' => '']);
        }
    }


    private function signing_order(&$requestData)
    {

        if (isset($requestData['signing_order'])) {
            $requestData['roles'][0]['index'] = 1;
            $requestData['roles'][1]['index'] = 2;

        }
    }

    private function signerSession($packageId)
    {
        $response = $this->makeRequest()->asJson()->post('/api/authenticationTokens/sender', ['packageId' => $packageId]);
        $token = $response['value'];

        $url = url('https://try.validsign.eu/auth?senderAuthenticationToken=' . $token . '&target=https://try.validsign.eu/a/transaction/' . $packageId . '/designer');

        return "<script>window.open('$url', '_blank');</script>";
    }

    private function me_as_signer(&$requestData, $packageId): void
    {

        if (isset($requestData['me_as_signer'])) {
            $this->makeRequest()->put('/api/packages/' . $packageId, ['data' => ['senderVisible' => true]]);

        }
    }

    /**
     * @param $requestData
     * @return void
     */
    private function sms_verification(&$requestData): void
    {
        if (isset($requestData['sms_verification_signer1'])) {

            $requestData['roles'][0]['signers'][0]['auth'] = [
                'scheme' => 'SMS',
                'challenges' => [
                    [
                        "answer" => null,
                        "question" => "+31612345678",
                        "maskInput" => false
                    ]
                ]
            ];
        }


        if (isset($requestData['sms_verification_signer2'])) {

            $requestData['roles'][1]['signers'][0]['auth'] = [
                'scheme' => 'SMS',
                'challenges' => [
                    [
                        "answer" => null,
                        "question" => "+31622345678",
                        "maskInput" => false
                    ]
                ]
            ];
        }
    }

    public function download_document()
    {
        $response = $this->makeRequest()->get('/api/packages/');

        $packageId = $response->json('results')[0]['id'];
        $response = $this->makeRequest()->accept('application/pdf')->get('/api/packages/' . $packageId);

        $document_id = $response->json('documents')[0]['id'];

        $response1 = $this->makeRequest()->accept('application/pdf')->get('/api/packages/' . $packageId . '/documents/' . $document_id . '/pdf');
        $content = $response1->body();

        return Response::make($content, 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'attachment; filename="document.pdf"',
        ]);

    }

    public function summary($packageId)
    {
        $response = $this->makeRequest()->get('/api/packages/');


        $response = $this->makeRequest()->accept('application/pdf')->get('/api/packages/' . $packageId . '/evidence/summary');
        $content = $response->body();

        return Response::make($content, 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'attachment; filename="evidence_summary.pdf"',
        ]);
    }

    public function callback(Request $request)
    {
        $payload = $request->payload;

        $data = json_decode($payload, true);

        $packageId = $data['packageId'];
        if ($data['name'] == "PACKAGE_COMPLETE") {
            return $this->summary($packageId);
        } else {
            return response()->json(['error' => 'package is not completed'], 400);
        }


    }


    /**
     * Create Http Request Instance.
     *
     * @return PendingRequest
     */
    private function makeRequest(): PendingRequest
    {
        return Http::withHeaders([
            'Authorization' => 'Basic ' . env('VALIDSIGN_API_KEY'),
            'Accept' => 'application/json'
        ])->baseUrl('https://try.validsign.eu');

    }
}

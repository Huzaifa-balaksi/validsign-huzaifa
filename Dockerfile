FROM php:8.2-apache

RUN apt-get update

# 1. development packages
RUN apt-get install -y \
    curl \
    g++ \
    git \
    gpg \
    libbz2-dev \
    libfreetype6-dev \
    libicu-dev \
    libjpeg-dev \
    libmcrypt-dev \
    libpng-dev \
    libreadline-dev \
    libxml2-dev \
    libzip-dev \
    lsb-release \
    npm \
    ssl-cert \
    sudo \
    unzip \
    vim \
    wget \
    zip

# For converting documents to pdf.
RUN apt-get install -y --no-install-recommends \
    default-jre \
    libreoffice-java-common \
    libreoffice

RUN pecl install redis-5.3.7 \
    && docker-php-ext-enable redis

# 4. start with base php config, then add extensions
RUN mv "$PHP_INI_DIR/php.ini-development" "$PHP_INI_DIR/php.ini"

RUN docker-php-ext-install \
    gd \
    bz2 \
    intl \
    exif \
    soap \
    pcntl \
    iconv \
    bcmath \
    opcache \
    calendar \
    pdo_mysql \
    zip

# Config apache
RUN groupadd -g 1000 www
RUN useradd -u 1000 -ms /bin/bash -g www www
COPY --chown=www:www . /var/www/html
RUN mkdir -p /etc/httpd/logs/
RUN chown -R www:www /etc/httpd/logs/
COPY ./docker/apache/default.conf /etc/apache2/sites-available/000-default.conf

RUN sed -ri -e 's!Listen 80!Listen 8080!g' /etc/apache2/ports.conf

# Install xDebug (only for local development)
ARG INSTALL_XDEBUG=false
RUN if [ "$INSTALL_XDEBUG" = "true" ]; then \
    pecl install xdebug \
    && docker-php-ext-enable xdebug; \
fi

# Make sure all debian packages are up to date (the image doesn't always ensure this)
RUN apt-get upgrade -y

# 3. mod_rewrite for URL rewrite and mod_headers for .htaccess extra headers like Access-Control-Allow-Origin-
RUN a2enmod rewrite headers

# Optimize production builds only, not local ones.
ARG ENVIRONMENT=production
RUN if [ "$ENVIRONMENT" = "production" ]; then \
    php artisan event:cache \
    && php artisan route:cache \
    && php artisan view:cache; \
fi

USER www

EXPOSE 8080
